import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireAuth } from '@angular/fire/auth';







@Injectable({
  providedIn: 'root'
})
export class AuthService {

  

  user: Observable<firebase.User>;


  signup(email:string,password:string){
    return this.fireBaseAuth
     .auth
     .createUserWithEmailAndPassword(email,password);
   }

   updateProfile(user, name:string) {
    user.updateProfile({displayName: name, photoURL: ''});
  }

  login(email: string, password: string) {
    return this.fireBaseAuth.auth.signInWithEmailAndPassword(email, password);
  } 

  logOut() {
    return this.fireBaseAuth.auth.signOut();
  }
 
 
  constructor(private fireBaseAuth:AngularFireAuth,){ 
    this.user = fireBaseAuth.authState;

  }
}
