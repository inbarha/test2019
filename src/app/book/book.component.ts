import { Component, OnInit, Input } from '@angular/core';
import { BooksService } from '../books.service';



@Component({
  selector: 'book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {
  @Input() data:any;
  name:string;
  author:string;
  key;
  showTheButton=false;
  showEditField = false;
 tempText;
 checkboxFlag: boolean;

 
  
  

   constructor(private booksService:BooksService) { }

   showButton(){
    this.showTheButton = true;
 
  }
  hideButton(){
    this.showTheButton = false;
 
  }
  checkChange()  {
    this.booksService.updateDone(this.key,this.name,this.checkboxFlag);
   }
  

  showEdit() {
    this.showEditField = true;
     this.tempText = this.name;

  }
 
  save() {
    this.booksService.update(this.key, this.name,this.author);
    this.showEditField = false;
  }
 
  cancel() {
    this.showEditField = false;
    this.name = this.tempText;
    this.author = this.tempText;
  }
 
  ngOnInit() {
    this.name = this.data.name;
    this.author = this.data.author;
    this.key = this.data.$key;
    this.checkboxFlag = this.data.done;


  }
 
}
