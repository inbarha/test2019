import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { AngularFireDatabase } from '@angular/fire/database';



@Injectable({
  providedIn: 'root'
})
export class BooksService {

  addBook(name,author){ //מקבלים טקסט מהTS

    this.authService.user.subscribe(user=>{
      if(!user) return;
      this.db.list('/user/'+user.uid+'/books/').push({'name':name, 'author':author});//דוחפים לפיירבייס
    })
  }
 
  update(key, name, author) {
    this.authService.user.subscribe(
      user => {
        if(!user) return;
        this.db.list('user/'+user.uid+'/books').update(key,{'name':name,'author':author});
      }
    )
  }

  
updateDone(key:string, name:string, done:boolean)
{
  this.authService.user.subscribe(user =>{
    if(!user) return;
    this.db.list('/user/'+user.uid+'/books').update(key,{'name':name, 'done':done});
 })

}

 

  constructor(private authService:AuthService,
    private db:AngularFireDatabase,
  ) { }

}
