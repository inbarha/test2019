import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { BooksService } from '../books.service';
import { AngularFireDatabase } from '@angular/fire/database';



@Component({
  selector: 'books',
  templateUrl: './books.component.html',
  styleUrls: ['./books.component.css']
})
export class BooksComponent implements OnInit {

  name:string;// להוספה 
  author:string;//
  books=[];//להצגה


  addproduct(name:string,color:string,price:number,currency:string){
    this.booksService.addBook(this.name,this.author);
  
    this.name='';// מרוקן את התיבה
    this.author='';// מרוקן את התיבה
   
    }
  constructor(private db:AngularFireDatabase,
    public authService:AuthService,
    private booksService:BooksService) { }

    
   
 
   
    ngOnInit() {
        this.authService.user.subscribe(
          user => {
            if(!user) return;
            this.db.list('/user/'+user.uid+'/books').snapshotChanges().subscribe(
                    books => {
                   this.books = [];
                    books.forEach(
                      book =>{
                         let y =book.payload.toJSON();
                          y["$key"] = book.key;
                          this.books.push(y);
                       }
                     )
                  }
                )
                }
        )
  }

}



