import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router"
import { AuthService } from '../auth.service';



@Component({
  selector: 'nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.css']
})
export class NavComponent implements OnInit {

  toBooks() {
    this.router.navigate(['/books'])
  }
 
  toLogin() {
    this.router.navigate(['/login'])
  }
 
  toRegister() {
    this.router.navigate(['/register'])
  }
 
  logOut() {
    this.authService.logOut()
    .then(value => {
      this.router.navigate(['/login'])
    }).catch(err => {
      console.log(err)
    });
  }
 

  constructor(private router:Router,public authService:AuthService ) { }

  ngOnInit() {
  }

}
