import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { Router } from '@angular/router';
import { AngularFireDatabase } from '@angular/fire/database';
import {FormControl, Validators} from '@angular/forms';





@Component({
  selector: 'register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  name: string;
  password: string;
  username:string;
  code = '';
  message ='';
 
  nameFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  emailFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.email,
  ]);
 
  signup() {
    this.authService.signup(this.email, this.password)
    .then(value => {
      this.authService.updateProfile(value.user, this.name);
    }).then(value => {
      this.router.navigate(['/']);
    }).catch(err => {
      this.code = err.code;
      this.message = err.message;
      console.log(err);
   
    });
  }
 



 
 
  constructor(private authService:AuthService,private router:Router, private db:AngularFireDatabase,)
  { }

  ngOnInit() {
  }

}
