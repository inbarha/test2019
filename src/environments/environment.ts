// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyBoIuRHdfGTroHBb7Wux_frRYkcmTzNlPU",
    authDomain: "test2019-ccace.firebaseapp.com",
    databaseURL: "https://test2019-ccace.firebaseio.com",
    projectId: "test2019-ccace",
    storageBucket: "test2019-ccace.appspot.com",
    messagingSenderId: "246668038499"
  }
 };
 

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
